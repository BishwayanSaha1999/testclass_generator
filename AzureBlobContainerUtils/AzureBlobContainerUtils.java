
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.azure.core.util.BinaryData;
import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.blob.models.BlobItem;
import com.azure.storage.blob.models.PublicAccessType;

@Component
public class AzureBlobContainerUtils {

	@Value("${storage-connection-string}")
	private String storageConnectionString;

	public void uploadBlobToAzure(String policyId, byte[] fileByteArr, String fileName) {
		BlobContainerClient container = getBlobContainer(policyId.toLowerCase());
		List<String> blobs = listOfBlobNames(policyId);
		if (blobs.contains(fileName)) {
			deleteBlock(container, fileName);
		}
		uploadFile(container, fileName, fileByteArr);
	}

	public List<String> listOfBlobNames(String policyId) {
		List<BlobItem> blobItems = fetchAllBlobFromContainer(policyId);
		List<String> blobItemNames = new ArrayList<>();
		blobItems.forEach(blob -> blobItemNames.add(blob.getName()));
		return blobItemNames;
	}

	public List<BlobItem> fetchAllBlobFromContainer(String policyId) {
		BlobContainerClient container = getBlobContainer(policyId.toLowerCase());
		List<BlobItem> listOfBlobs = new ArrayList<>();
		// List the blob(s) in the container.
		for (BlobItem blobItem : container.listBlobs()) {
			listOfBlobs.add(blobItem);
		}
		return listOfBlobs;
	}

	public BlobContainerClient createBlobContainer(String containerName) {
		BlobServiceClient blobServiceClient = new BlobServiceClientBuilder().connectionString(storageConnectionString).buildClient();
		return blobServiceClient.createBlobContainer(containerName);
	}
	
	public BlobContainerClient getBlobContainer(String containerName) { 
		BlobServiceClient blobServiceClient = new BlobServiceClientBuilder().connectionString(storageConnectionString).buildClient();
		BlobContainerClient blobContainerClient = blobServiceClient.getBlobContainerClient(containerName).exists()
				? blobServiceClient.getBlobContainerClient(containerName)
				: blobServiceClient.createBlobContainer(containerName);
		blobContainerClient.setAccessPolicy(PublicAccessType.CONTAINER, null);

		return blobContainerClient;
	}
	
	public void uploadFile(BlobContainerClient blobContainerClient,String blockName,byte[] fileData) {

		BlobClient blobClient = blobContainerClient.getBlobClient(blockName);
		BinaryData binaryData = BinaryData.fromBytes(fileData);
		blobClient.upload(binaryData);
	}

	public byte[] getFileData(BlobContainerClient blobContainerClient, String blockName) {
		BlobClient blobClient = blobContainerClient.getBlobClient(blockName);
		BinaryData binaryData = blobClient.downloadContent();
		return binaryData.toBytes();
	}

	public String getFileUrl(BlobContainerClient blobContainerClient, String blockName) {
		BlobClient blobClient = blobContainerClient.getBlobClient(blockName);
		return blobClient.getBlobUrl();
	}

	public void deleteContainer(BlobContainerClient blobContainerClient) {
		blobContainerClient.delete();
	}

	public void deleteBlock(BlobContainerClient blobContainerClient, String blockName) {
		BlobClient blobClient = blobContainerClient.getBlobClient(blockName);
		blobClient.delete();
	}

	public List<byte[]> getFileDataByContainer(BlobContainerClient blobContainerClient) {
		List<byte[]> fileDataList = new ArrayList<>();
		for (BlobItem blobItem : blobContainerClient.listBlobs()) {
			BlobClient blobClient = blobContainerClient.getBlobClient(blobItem.getName());
			fileDataList.add(blobClient.downloadContent().toBytes());
		}
		return fileDataList;
	}
}
