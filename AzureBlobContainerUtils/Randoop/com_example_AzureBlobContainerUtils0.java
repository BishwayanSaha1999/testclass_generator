import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class com_example_AzureBlobContainerUtils0 {

    public static boolean debug = false;

    @Test
    public void test01() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test01");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        // The following exception was thrown during execution in test generation
        try {
            byte[] byteArray3 = azureBlobContainerUtils0.getFileData(blobContainerClient1, "");
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test02() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test02");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        // The following exception was thrown during execution in test generation
        try {
            byte[] byteArray3 = azureBlobContainerUtils0.getFileData(blobContainerClient1, "hi!");
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test03() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test03");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        // The following exception was thrown during execution in test generation
        try {
            java.util.List<java.lang.String> strList2 = azureBlobContainerUtils0.listOfBlobNames("");
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: Invalid connection string.");
        } catch (java.lang.IllegalArgumentException e) {
            // Expected exception.
        }
    }

    @Test
    public void test04() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test04");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        // The following exception was thrown during execution in test generation
        try {
            java.util.List<java.lang.String> strList2 = azureBlobContainerUtils0.listOfBlobNames("hi!");
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: Invalid connection string.");
        } catch (java.lang.IllegalArgumentException e) {
            // Expected exception.
        }
    }

    @Test
    public void test05() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test05");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        java.lang.Class<?> wildcardClass1 = azureBlobContainerUtils0.getClass();
    }

    @Test
    public void test06() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test06");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        byte[] byteArray3 = null;
        // The following exception was thrown during execution in test generation
        try {
            azureBlobContainerUtils0.uploadFile(blobContainerClient1, "hi!", byteArray3);
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test07() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test07");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        // The following exception was thrown during execution in test generation
        try {
            com.azure.storage.blob.BlobContainerClient blobContainerClient2 = azureBlobContainerUtils0.getBlobContainer("");
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: Invalid connection string.");
        } catch (java.lang.IllegalArgumentException e) {
            // Expected exception.
        }
    }

    @Test
    public void test08() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test08");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        // The following exception was thrown during execution in test generation
        try {
            com.azure.storage.blob.BlobContainerClient blobContainerClient2 = azureBlobContainerUtils0.createBlobContainer("");
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: Invalid connection string.");
        } catch (java.lang.IllegalArgumentException e) {
            // Expected exception.
        }
    }

    @Test
    public void test09() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test09");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        // The following exception was thrown during execution in test generation
        try {
            java.util.List<byte[]> byteArrayList2 = azureBlobContainerUtils0.getFileDataByContainer(blobContainerClient1);
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test10() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test10");
        java.lang.Object obj0 = new java.lang.Object();
        java.lang.Class<?> wildcardClass1 = obj0.getClass();
    }

    @Test
    public void test11() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test11");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        // The following exception was thrown during execution in test generation
        try {
            com.azure.storage.blob.BlobContainerClient blobContainerClient2 = azureBlobContainerUtils0.createBlobContainer("hi!");
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: Invalid connection string.");
        } catch (java.lang.IllegalArgumentException e) {
            // Expected exception.
        }
    }

    @Test
    public void test12() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test12");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        // The following exception was thrown during execution in test generation
        try {
            java.lang.String str3 = azureBlobContainerUtils0.getFileUrl(blobContainerClient1, "");
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test13() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test13");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        // The following exception was thrown during execution in test generation
        try {
            java.util.List<com.azure.storage.blob.models.BlobItem> blobItemList2 = azureBlobContainerUtils0.fetchAllBlobFromContainer("");
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: Invalid connection string.");
        } catch (java.lang.IllegalArgumentException e) {
            // Expected exception.
        }
    }

    @Test
    public void test14() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test14");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        // The following exception was thrown during execution in test generation
        try {
            java.util.List<com.azure.storage.blob.models.BlobItem> blobItemList2 = azureBlobContainerUtils0.fetchAllBlobFromContainer("hi!");
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: Invalid connection string.");
        } catch (java.lang.IllegalArgumentException e) {
            // Expected exception.
        }
    }

    @Test
    public void test15() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test15");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        byte[] byteArray7 = new byte[] { (byte) 10, (byte) 0, (byte) 0, (byte) -1 };
        // The following exception was thrown during execution in test generation
        try {
            azureBlobContainerUtils0.uploadFile(blobContainerClient1, "", byteArray7);
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test16() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test16");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        byte[] byteArray4 = new byte[] { (byte) -1 };
        // The following exception was thrown during execution in test generation
        try {
            azureBlobContainerUtils0.uploadFile(blobContainerClient1, "hi!", byteArray4);
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test17() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test17");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        byte[] byteArray7 = new byte[] { (byte) 0, (byte) 1, (byte) 0, (byte) 1 };
        // The following exception was thrown during execution in test generation
        try {
            azureBlobContainerUtils0.uploadFile(blobContainerClient1, "hi!", byteArray7);
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test18() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test18");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        byte[] byteArray8 = new byte[] { (byte) 10, (byte) 0, (byte) 10, (byte) 1, (byte) -1 };
        // The following exception was thrown during execution in test generation
        try {
            azureBlobContainerUtils0.uploadFile(blobContainerClient1, "hi!", byteArray8);
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test19() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test19");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        byte[] byteArray4 = new byte[] { (byte) 100, (byte) 100 };
        // The following exception was thrown during execution in test generation
        try {
            azureBlobContainerUtils0.uploadBlobToAzure("", byteArray4, "");
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: Invalid connection string.");
        } catch (java.lang.IllegalArgumentException e) {
            // Expected exception.
        }
    }

    @Test
    public void test20() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test20");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        byte[] byteArray3 = new byte[] {};
        // The following exception was thrown during execution in test generation
        try {
            azureBlobContainerUtils0.uploadFile(blobContainerClient1, "", byteArray3);
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test21() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test21");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        // The following exception was thrown during execution in test generation
        try {
            com.azure.storage.blob.BlobContainerClient blobContainerClient2 = azureBlobContainerUtils0.getBlobContainer("hi!");
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: Invalid connection string.");
        } catch (java.lang.IllegalArgumentException e) {
            // Expected exception.
        }
    }

    @Test
    public void test22() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test22");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        // The following exception was thrown during execution in test generation
        try {
            java.lang.String str3 = azureBlobContainerUtils0.getFileUrl(blobContainerClient1, "hi!");
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test23() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test23");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        // The following exception was thrown during execution in test generation
        try {
            azureBlobContainerUtils0.deleteContainer(blobContainerClient1);
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test24() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test24");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        byte[] byteArray3 = new byte[] { (byte) 0 };
        // The following exception was thrown during execution in test generation
        try {
            azureBlobContainerUtils0.uploadBlobToAzure("", byteArray3, "hi!");
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: Invalid connection string.");
        } catch (java.lang.IllegalArgumentException e) {
            // Expected exception.
        }
    }

    @Test
    public void test25() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test25");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        byte[] byteArray4 = new byte[] { (byte) 0 };
        // The following exception was thrown during execution in test generation
        try {
            azureBlobContainerUtils0.uploadFile(blobContainerClient1, "hi!", byteArray4);
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test26() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test26");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        // The following exception was thrown during execution in test generation
        try {
            azureBlobContainerUtils0.deleteBlock(blobContainerClient1, "");
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test27() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_AzureBlobContainerUtils0.test27");
        com.example.AzureBlobContainerUtils azureBlobContainerUtils0 = new com.example.AzureBlobContainerUtils();
        com.azure.storage.blob.BlobContainerClient blobContainerClient1 = null;
        byte[] byteArray3 = new byte[] {};
        // The following exception was thrown during execution in test generation
        try {
            azureBlobContainerUtils0.uploadFile(blobContainerClient1, "hi!", byteArray3);
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }
}

