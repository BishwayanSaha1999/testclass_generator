import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class com_azure_storage_blob_BlobContainerClient0 {

    public static boolean debug = false;

    @Test
    public void test1() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_azure_storage_blob_BlobContainerClient0.test1");
        java.lang.String str0 = com.azure.storage.blob.BlobContainerClient.LOG_CONTAINER_NAME;
    }

    @Test
    public void test2() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_azure_storage_blob_BlobContainerClient0.test2");
        java.lang.Object obj0 = new java.lang.Object();
        java.lang.Class<?> wildcardClass1 = obj0.getClass();
    }

    @Test
    public void test3() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_azure_storage_blob_BlobContainerClient0.test3");
        java.lang.String str0 = com.azure.storage.blob.BlobContainerClient.STATIC_WEBSITE_CONTAINER_NAME;
    }

    @Test
    public void test4() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_azure_storage_blob_BlobContainerClient0.test4");
        java.lang.String str0 = com.azure.storage.blob.BlobContainerClient.ROOT_CONTAINER_NAME;
    }
}

