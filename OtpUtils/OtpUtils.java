import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.time.Instant;
import java.time.temporal.ChronoField;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class OtpUtils {

	private static Random rand = new Random();

	private OtpUtils() {

	}

	/**
	 * <p>
	 * Get Random 4digitNumber
	 * </p>
	 * 
	 * @return Integer
	 */
	public static Integer getRandom4Digit() {
		return rand.nextInt(9000) + 1000;
	}

	/**
	 * This method uses the JCE to provide the crypto algorithm. HMAC computes a
	 * Hashed Message Authentication Code with the crypto hash algorithm as a
	 * parameter.
	 *
	 * @param crypto   the crypto algorithm (HmacSHA1, HmacSHA256, HmacSHA512)
	 * @param keyBytes the bytes to use for the HMAC key
	 * @param text     the message or text to be authenticated.
	 */
	private static byte[] hmacSha1(String crypto, byte[] keyBytes, byte[] text) {
		try {
			Mac hmac;
			hmac = Mac.getInstance(crypto);
			SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
			hmac.init(macKey);
			return hmac.doFinal(text);
		} catch (GeneralSecurityException gse) {
			throw new UndeclaredThrowableException(gse);
		}
	}

	/**
	 * This method converts HEX string to Byte[]
	 *
	 * @param hex the HEX string
	 *
	 * @return A byte array
	 */
	private static byte[] hexStr2Bytes(String hex) {
		// Adding one byte to get the right conversion
		// values starting with "0" can be converted
		byte[] bArray = new BigInteger("10" + hex, 16).toByteArray();

		// Copy all the REAL bytes, not the "first"
		byte[] ret = new byte[bArray.length - 1];
		for (int i = 0; i < ret.length; i++)
			ret[i] = bArray[i + 1];
		return ret;
	}

	private static final int[] DIGITS_POWER
	// 0 1 2 3 4 5 6 7 8
			= { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000 };

	/**
	 * This method generates an TOTP value for the given set of parameters.
	 *
	 * @param key          the shared secret, HEX encoded
	 * @param time         a value that reflects a time
	 * @param returnDigits number of digits to return
	 *
	 * @return A numeric String in base 10 that includes {@link truncationDigits}
	 *         digits
	 */
	public static String generateTOTP(String key, String time, String returnDigits) {
		return generateTOTP(key, time, returnDigits, "HmacSHA1");
	}

	/**
	 * This method generates an TOTP value for the given set of parameters.
	 *
	 * @param key          the shared secret, HEX encoded
	 * @param time         a value that reflects a time
	 * @param returnDigits number of digits to return
	 *
	 * @return A numeric String in base 10 that includes {@link truncationDigits}
	 *         digits
	 */
	public static String generateTOTP256(String key, String time, String returnDigits) {
		return generateTOTP(key, time, returnDigits, "HmacSHA256");
	}

	/**
	 * This method generates an TOTP value for the given set of parameters.
	 *
	 * @param key          the shared secret, HEX encoded
	 * @param time         a value that reflects a time
	 * @param returnDigits number of digits to return
	 *
	 * @return A numeric String in base 10 that includes {@link truncationDigits}
	 *         digits
	 */
	public static String generateTOTP512(String key, String time, String returnDigits) {
		return generateTOTP(key, time, returnDigits, "HmacSHA512");
	}

	/**
	 * This method generates an TOTP value for the given set of parameters.
	 *
	 * @param key          the shared secret, HEX encoded
	 * @param time         a value that reflects a time
	 * @param returnDigits number of digits to return
	 * @param crypto       the crypto function to use
	 *
	 * @return A numeric String in base 10 that includes {@link truncationDigits}
	 *         digits
	 */
	private static String generateTOTP(String key, String time, String returnDigits, String crypto) {
		int codeDigits = Integer.decode(returnDigits).intValue();
		String result = null;
		byte[] hash;
		StringBuilder timeBld = new StringBuilder();
		StringBuilder resultBld = new StringBuilder();

		// Using the counter
		// First 8 bytes are for the movingFactor
		// Complaint with base RFC 4226 (HOTP)
		while (time.length() < 16) {
			timeBld = timeBld.append("0").append(time);
			time = timeBld.toString();
		}
		// Get the HEX in a Byte[]
		byte[] msg = hexStr2Bytes(time);

		// Adding one byte to get the right conversion
		byte[] k = hexStr2Bytes(key);

		hash = hmacSha1(crypto, k, msg);

		// put selected bytes into result int
		int offset = hash[hash.length - 1] & 0xf;

		int binary = ((hash[offset] & 0x7f) << 24) | ((hash[offset + 1] & 0xff) << 16)
				| ((hash[offset + 2] & 0xff) << 8) | (hash[offset + 3] & 0xff);

		int otp = binary % DIGITS_POWER[codeDigits];

		result = Integer.toString(otp);
		while (result.length() < codeDigits) {
			resultBld = resultBld.append("0").append(result);
			result = resultBld.toString();
		}

		return result;
	}

	public static byte[] longToBytes(long l) {
		byte[] result = new byte[Long.BYTES];
		for (int i = Long.BYTES - 1; i >= 0; i--) {
			result[i] = (byte) (l & 0xFF);
			l >>= Byte.SIZE;
		}
		return result;
	}

	public static String getSteps(String durationToAlive) {

		long timeref = 60;
		String steps;
		StringBuilder stepsBld = new StringBuilder();

		// this is the time upto whhich OTP persist same value.
		long timeToAlive = Integer.valueOf(durationToAlive);
		long currentInstant = Instant.now().getLong(ChronoField.INSTANT_SECONDS);
		long calculatedVal = (currentInstant - timeref) / timeToAlive;
		steps = Long.toHexString(calculatedVal).toUpperCase();

		while (steps.length() < 16) {
			stepsBld = stepsBld.append("0").append(steps);
			steps = stepsBld.toString();
		}
		return steps;
	}

}
