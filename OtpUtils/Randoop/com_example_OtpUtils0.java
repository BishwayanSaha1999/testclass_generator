import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class com_example_OtpUtils0 {

    public static boolean debug = false;

    @Test
    public void test01() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test01");
        byte[] byteArray1 = com.example.OtpUtils.longToBytes((long) (short) 100);
    }

    @Test
    public void test02() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test02");
        // The following exception was thrown during execution in test generation
        try {
            java.lang.String str1 = com.example.OtpUtils.getSteps("");
            org.junit.Assert.fail("Expected exception of type java.lang.NumberFormatException; message: For input string: \"\"");
        } catch (java.lang.NumberFormatException e) {
            // Expected exception.
        }
    }

    @Test
    public void test03() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test03");
        java.lang.Integer int0 = com.example.OtpUtils.getRandom4Digit();
    }

    @Test
    public void test04() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test04");
        // The following exception was thrown during execution in test generation
        try {
            java.lang.String str3 = com.example.OtpUtils.generateTOTP512("", "hi!", "");
            org.junit.Assert.fail("Expected exception of type java.lang.NumberFormatException; message: Zero length string");
        } catch (java.lang.NumberFormatException e) {
            // Expected exception.
        }
    }

    @Test
    public void test05() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test05");
        // The following exception was thrown during execution in test generation
        try {
            java.lang.String str1 = com.example.OtpUtils.getSteps("hi!");
            org.junit.Assert.fail("Expected exception of type java.lang.NumberFormatException; message: For input string: \"hi!\"");
        } catch (java.lang.NumberFormatException e) {
            // Expected exception.
        }
    }

    @Test
    public void test06() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test06");
        byte[] byteArray1 = com.example.OtpUtils.longToBytes((long) 'a');
        java.lang.Class<?> wildcardClass2 = byteArray1.getClass();
    }

    @Test
    public void test07() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test07");
        // The following exception was thrown during execution in test generation
        try {
            java.lang.String str3 = com.example.OtpUtils.generateTOTP512("hi!", "hi!", "");
            org.junit.Assert.fail("Expected exception of type java.lang.NumberFormatException; message: Zero length string");
        } catch (java.lang.NumberFormatException e) {
            // Expected exception.
        }
    }

    @Test
    public void test08() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test08");
        java.lang.Object obj0 = new java.lang.Object();
        java.lang.Class<?> wildcardClass1 = obj0.getClass();
    }

    @Test
    public void test09() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test09");
        byte[] byteArray1 = com.example.OtpUtils.longToBytes(0L);
    }

    @Test
    public void test10() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test10");
        byte[] byteArray1 = com.example.OtpUtils.longToBytes(1L);
    }

    @Test
    public void test11() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test11");
        // The following exception was thrown during execution in test generation
        try {
            java.lang.String str3 = com.example.OtpUtils.generateTOTP256("", "hi!", "");
            org.junit.Assert.fail("Expected exception of type java.lang.NumberFormatException; message: Zero length string");
        } catch (java.lang.NumberFormatException e) {
            // Expected exception.
        }
    }

    @Test
    public void test12() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test12");
        byte[] byteArray1 = com.example.OtpUtils.longToBytes((long) ' ');
    }

    @Test
    public void test13() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test13");
        byte[] byteArray1 = com.example.OtpUtils.longToBytes(10L);
        java.lang.Class<?> wildcardClass2 = byteArray1.getClass();
    }

    @Test
    public void test14() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test14");
        // The following exception was thrown during execution in test generation
        try {
            java.lang.String str3 = com.example.OtpUtils.generateTOTP512("hi!", "hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type java.lang.NumberFormatException; message: For input string: \"hi!\"");
        } catch (java.lang.NumberFormatException e) {
            // Expected exception.
        }
    }

    @Test
    public void test15() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "com_example_OtpUtils0.test15");
        // The following exception was thrown during execution in test generation
        try {
            java.lang.String str3 = com.example.OtpUtils.generateTOTP("", "hi!", "");
            org.junit.Assert.fail("Expected exception of type java.lang.NumberFormatException; message: Zero length string");
        } catch (java.lang.NumberFormatException e) {
            // Expected exception.
        }
    }
}

